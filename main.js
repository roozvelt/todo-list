// массив записей
var data = (localStorage.getItem('todoList'))
  ? JSON.parse(localStorage.getItem('todoList'))
  : [];
  
var arhiv = (localStorage.getItem('completedList'))
  ? JSON.parse(localStorage.getItem('completedList'))
  : [];
  
// синхронизация localStorage и Массива JS
function storageSync() {
  localStorage.setItem('todoList', JSON.stringify(data));
  localStorage.setItem('completedList', JSON.stringify(arhiv));
};

// рисуем начальный список
renderTodoList();
renderCompletedList();

// Добавить новый элемент в список
document.getElementById('btn-create').addEventListener('click', function () {
  var value = document.getElementById('input-create').value;
  if (value) {
    addItemToDOM(value);
    document.getElementById('input-create').value = '';

    data.push(value); 
	storageSync();
  } 
});

function renderCompletedList() {
	 if (!arhiv.length) return;

  for (var i = 0; i < arhiv.length; i++) {
    var value = arhiv[i];
    addArhivToDOM(value);
};
  
}
function renderTodoList() {
  if (!data.length) return;

  for (var i = 0; i < data.length; i++) {
    var value = data[i];
    addItemToDOM(value);
  };
};

function addArhivToDOM(text) {
  var list = document.getElementById('completed-list');

  var item = document.createElement('li'); 
  item.classList = 'collection-item';
 
 var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
    </span>
  </div>`;
  
  item.innerHTML = listItemView;

  list.appendChild(item);
};

//Добавить удалённую активную задачу в завершённые
function addItemToCompletedList(value){
	var list = document.getElementById('completed-list');
	var item = document.createElement('li');
	item.classList = 'collection-item';
				
	var listItemView = `
		<div class="item">
			<span class="item-text">${value}</span>
			<span class="secondary-content">
			</span>
		</div>`;
	item.innerHTML = listItemView;
	list.appendChild(item);
	arhiv.push(value);
	storageSync()
};

function addItemToDOM(text) {
  var list = document.getElementById('todo-list');

  var item = document.createElement('li'); 
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
      <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
    </span>
  </div>`;
  item.innerHTML = listItemView;

  // добавим слушатель для удаления
  var buttonDelete = item.getElementsByClassName('item-btn-del')[0];
  buttonDelete.addEventListener('click', removeItem);

  list.appendChild(item);
};

function removeItem(e) {
  var item = this.parentNode.parentNode.parentNode;
  var list = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;
  data.splice(data.indexOf(value), 1);
	addItemToCompletedList(value);
  list.removeChild(item);
  storageSync();
};
